# KPZLab6-Chess

#### Principles

## Interface Segregation Principle (ISP) in Chess Game Code

### Definition
The Interface Segregation Principle (ISP) asserts that clients should not be forced to depend on interfaces they do not use. This means it is better to have many specific interfaces rather than one general interface.

### Application of ISP in the Code
**Responsibility Separation:**
In the chess game code, the properties of cells ([Cell](ChessBoard/Cell.cs#L14-L55)) are separated into multiple interfaces:
- `<IMoveInfo>` for information about possible moves.
- `<IAttackInfo>` for information about attacks.
- `<ICheckInfo>` for information about checks.

### Utilization of SRP in the Code

In the refactored chess game code, the Single Responsibility Principle (SRP) is adhered to. Each method in the `Game` class is responsible for a specific operation:

1. **InitializeButton:** Initializes the game board, including creating buttons and placing pieces on them. [code](Chess/Game.cs#L48-L63)
2. **SetBoardColor:** Sets the background color of buttons on the board. [code](Chess/Game.cs#L516-L525)
3. **UpdateBoardOccupancy:** Updates the occupancy state of cells on the board. [code](Chess/Game.cs#L377-L387)
4. **DrawPieces Method:**
The `DrawPieces` method adheres to the Single Responsibility Principle. It performs only one specific task - displaying pieces on the board and setting the background color for cells that are in check. This approach helps keep the code clean and understandable, as well as easier to test and modify in the future.[code](Chess/Game.cs#L88-L180)

## Don't Repeat Yourself (DRY) Principle in Chess Game Code

### Definition
The Don't Repeat Yourself (DRY) principle states that every piece of knowledge must have a single, unambiguous, authoritative representation within a system.

### Application of DRY in the Code
To avoid repetition of similar code, common functionality can be extracted into separate methods or classes and reused wherever necessary. This helps in maintaining a clean codebase and makes future changes easier.

## KISS Principle in Chess Game Code

### Definition
The KISS principle (Keep It Simple, Stupid) states that most systems work best if they are kept simple rather than made complicated.

### Application of KISS in the Code
The codebase adheres to this principle by using clear and concise methods and classes. By keeping the code straightforward, it becomes easier to understand, maintain, and extend in the future.


## YAGNI Principle in Chess Game Code

### Definition
The YAGNI principle (You Aren't Gonna Need It) states that you should not add functionality until it is necessary. Avoid writing extra code that might complicate the program without providing immediate value.

### Application of YAGNI in the Code
In the chess game code, the YAGNI principle is applied by focusing only on the currently required functionality. This means avoiding the temptation to add features or code for potential future needs, which helps keep the codebase lean and manageable.


#### Patterns

## Command Pattern in Chess Game Code

### Definition
The Command pattern encapsulates a request as an object, thereby allowing for the parameterization of clients with queues, requests, and operations. It also provides support for undoable operations.

### Application of the Command Pattern in the Code
The Command pattern is used in the `Command` namespace, where commands are encapsulated as objects. This allows for the parameterization of methods with different commands and the ability to queue, schedule, and undo commands.

## Interfaces

### Interfaces in Chess Game Code
Providing communication at the interface levels: Using interfaces allows you to connect objects at the interface level, rather than at the level of specific classes. This ensures weak dependencies between objects and makes the code easier to test and maintain.[code](ChessBoard/Cell.cs#L38-L55)

## Factory Method

### Factory Method in Chess Game Code
Factory Method: The factory method is implicitly used when creating Button objects in the `PopulateButtons()` method. The factory method allows you to create objects without specifying the specific class of this object, and uses an abstract interface or class to create objects.[code](Chess/Game.cs#L34-L47)

## Singleton

### Singleton in Chess Game Code
1. We have a private static field _instance that stores a single instance of the Board class.
2. The constructor of the Board class is private to prevent instantiation outside the class.
3. We have a static GetInstance method that checks if a Board instance already exists. If not, it creates a new one. Locking is used for thread safety.
4. When we already have a Board instance, GetInstance returns a reference to it.[code](ChessBoard/Board.cs#L27-L28), [code](ChessBoard/Board.cs#L44-L54)

## Observer

### Observer in Chess Game Code
The observer is used to notify other objects about changes in the Board object. When the state of the board changes (for example, when the state of cells on the board changes), the NotifyObservers() method is called, which notifies all subscribed observers of the change. Each observer that implements the IChessObserver interface has an Update() method that is called when a message is received from the observed object.[code](ChessBoard/Board.cs#L6-L25), [code](ChessBoard/IChessObserver.cs)

## Refactoring Techniques

1. Changing the names of methods:
• The `populateGrid` method has been renamed to `PopulateGrid` to conform to C# method naming standards.
• The `PopulateButtons` method was created to initialize the buttons in the grid to make the code more clear and organized.
2. Separation of functionality:
• The button creation logic was moved to a separate `PopulateButtons` method, which is responsible only for creating buttons.
• The logic of placing pieces on the board was moved to a separate `SetInitialPiecePositions` method, which makes the code more understandable and structured.
3. Calling methods in the constructor:
• In the `Game` constructor, a sequential method call was displayed, which allows you to better understand the order of operations when creating a game object.
4. General logic:
• Changes in the general logic have not been made, but the refactoring allows a better understanding of what happens at each stage of game creation.
5. Separation of functionality:
• Previously, the `populateGrid()` method performed two tasks: creating buttons and setting the initial positions of shapes. Split this functionality into two separate methods: `PopulateButtons()` and `SetInitialPiecePositions()`. Now each method is responsible for its specific task, which makes the code easier to read and edit.
6. Improvement of variable and method names:
• Variables and methods have been renamed to better reflect their functionality and be more understandable to other developers. For example, `btnGrid` has been renamed to `btnGrid`, which better reflects its purpose.
7. Code optimization for better readability and performance:
• Some loops and methods have been optimized to reduce redundant code and improve performance. For example, instead of iterating through all the board cells to set the background color, a loop is applied with the appropriate color for each cell.
8. Using an object-oriented approach:
• ButtonTag object was used to store additional data about each button. This allows you to store information about the position of each button, which makes it easier to work with them.
9. Separation of functionality into methods:
• The code has been split into methods to reduce the length of each method and make them more understandable. For example, the functionality for setting the background color of the buttons was moved to a separate `SetBoardColor()` method.
10. Error correction:
• Fixed some bugs and flaws in the program's logic, such as incorrect definition of allowed moves and capture of pieces.
11. Removing redundant code:
• Redundant code that was irrelevant or unused was removed. For example, the redundant code responsible for displaying legal moves on the board was removed and moved to the `ShowLegalMoves` method.
12. Reorganization of methods and internal classes:
• Some methods and classes have been regrouped and restructured to improve code organization and make it more structured and easy to understand. For example, the logic for checking the end of the game has been moved to a separate `CheckGameEnd()` method.

