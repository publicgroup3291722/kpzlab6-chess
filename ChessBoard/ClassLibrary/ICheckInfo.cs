﻿namespace ChessBoard.ClassLibrary
{
    public interface ICheckInfo
    {
        bool IsCheckPath { get; set; }
        bool IsCheckPiece { get; set; }
    }
}
