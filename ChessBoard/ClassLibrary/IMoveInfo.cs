﻿namespace ChessBoard.ClassLibrary
{
    public interface IMoveInfo
    {
        bool IsLegalMove { get; set; }
        int LegalMovesCounter { get; set; }
    }
}
